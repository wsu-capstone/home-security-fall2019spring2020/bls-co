/*!
 *  vx_os.h
 *
 *  Handles collecting battery, temperature data and sends via LoRaWAN.
 *  Created on: Aug 7, 2019
 *  Author: Viaanix
 *
 */

#ifndef _VX_OS_H_
#define _VX_OS_H_

#include <stdbool.h>
#include <stdint.h>

#define VX_OS_LORAWAN_PORT (2)

typedef struct
{
	uint16_t battery_adc;
	uint16_t battery_level;
	uint16_t temperature_adc;
	uint32_t firmware_version;
	uint16_t carbonmonoxide_adc;
	uint32_t carbonmonoxide_level;
	uint8_t flags;
} vx_os_data_type;

typedef struct
{
	uint16_t check_in_time_minutes;
} vx_os_setting_type;

/*!
 * @brief  Initializes timer and state machine for VX OS.
 *
 * @param  None
 * @retval None
 *
 */
void VxOs_Init(void);

/*!
 * @brief  Runs the state machine for VX OS Processes.
 *
 * @param  None
 * @retval None
 *
 */
void VxOs_Process(void);

/*!
 * @brief  Checks to see if any data is ready to be sent.
 *
 * @param  data - vx_os_data_type pointer to be a container for any valid data.
 * @retval bool
 *         true - there is data to send.
 *         false - there is no data to send, ignore data container.
 *
 */
bool VxOs_IsDataToSend(vx_os_data_type* data);

/*!
 * @brief  Is called when there is RX data from LoRa Handler.
 *
 * @param  [IN] data - uint8_t ptr to buffer containing data.
 * @param  [IN] size - uint8_t size of data in buffer.
 * @retval bool
 *         true - able to handle rx data.
 *         false - something with handling rx data.
 *
 */
bool VxOs_HandleRx(uint8_t* data, uint8_t size);

/*!
 * @brief  Starts the OS process (timer) for collecting data for check-ins.
 *         If already running, restarts timer.
 *
 * @param  None
 * @retval None
 *
 */
void VxOs_StartProcess(void);

/*!
 * @brief  Stops the OS process (timer) for collecting data for check-ins.
 *
 * @param  None
 * @retval None
 *
 */
void VxOs_StopProcess(void);

/*!
 * @brief  Lets caller know if the OS is currently processing something.
 *
 * @param  None
 * @retval bool
 *         true - Is processing something. Please don't go to sleep.
 *         false - Is not processing anything, go ahead and sleep.
 *
 */
bool VxOs_IsBusy(void);

#endif
