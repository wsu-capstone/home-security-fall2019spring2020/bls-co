/* ========================================
 * File Name: fifo.h
 * Include this file to create fifo buffers of certain sizes.
 * To create a buffer you will call AddPointerFifo(name,size,type,pass_status,fail_status)
 * Example: AddPointerFifo(Spi,20,uint8, 1, 0);
 * This will make a pointer fifo buffer with size 20 containing types uint8
 * 1 is the return status if the fifo get and put functions are successful while 0 means fail.
 * The follwoing functions would be created: 
 * SpiFifo_Init(void), 
 * SpiFifo_Get(uint8* data),
 * SpiFifo_Put(uint8 data)
 * If the buffer is empty, Fifo_Get will return the fail status.
 * If the buffer is full, Fifo_Put will retun the fail status.
 * Otherwise, the status returned will be success.
 * You need to call NameFifo_Init before the first get or put is called.
 * ========================================
*/

/* This example accompanies the book
   "Embedded Systems: Real Time Interfacing to ARM Cortex M Microcontrollers",
   ISBN: 978-1463590154, Jonathan Valvano, copyright (c) 2015
   Programs 3.7, 3.8., 3.9 and 3.10 in Section 3.7

 Copyright 2015 by Jonathan W. Valvano, valvano@mail.utexas.edu
    You may use, edit, run or distribute this file
    as long as the above copyright notice remains
 THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 VALVANO SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/
 */

#ifndef _FIFO_H_
#define _FIFO_H_

#define AddPointerFifo(NAME,SIZE,TYPE,SUCCESS,FAIL) 	\
TYPE volatile *NAME ## PutPt;						              \
TYPE volatile *NAME ## GetPt;													\
TYPE static NAME ## Fifo [SIZE];											\
void NAME ## Fifo_Init(void) {												\
	NAME ## PutPt = NAME ## GetPt = &NAME ## Fifo[0];		\
}																											\
int NAME ## Fifo_Put (TYPE data){											\
	TYPE volatile *nextPutPt;														\
	nextPutPt = NAME ## PutPt + 1;											\
	if(nextPutPt == &NAME ## Fifo[SIZE]){								\
		nextPutPt = &NAME ## Fifo[0];											\
	}																										\
	if(nextPutPt == NAME ## GetPt){											\
		return(FAIL);																			\
	}																										\
	else{																								\
		*(NAME ## PutPt) = data;													\
		NAME ## PutPt = nextPutPt;												\
		return(SUCCESS);																	\
	}																										\
}																											\
int NAME ## Fifo_Get (TYPE *datapt){									\
	if(NAME ## PutPt == NAME ## GetPt){									\
		return(FAIL);																			\
	}																										\
	*datapt = *(NAME ## GetPt);													\
	NAME ## GetPt += 1;																	\
	if( NAME ## GetPt == &NAME ## Fifo[SIZE]){					\
		NAME ## GetPt = &NAME ## Fifo[0];									\
	}																										\
	return(SUCCESS);																		\
}																											

#endif
