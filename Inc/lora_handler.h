/*!
 * lora_handler.h
 *
 *  Created on: Aug 4, 2019
 *  Author: Viaanix
 *
 */

#ifndef _LORA_HANDLER_H_
#define _LORA_HANDLER_H_

#include <stdint.h>
#include <stdbool.h>

void LoraHandler_Init(void);

void LoraHandler_Process(void);

bool LoraHandler_IsBusy(void);

#endif
