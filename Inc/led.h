/*!***************************************************************************
  * @file    led.h
  * @author  Viaanix
  * @version V1.0.0
  * @date    21-April-2019
  * @brief   handles states of LEDs.
  ****************************************************************************
  */
  
#ifndef _LED_H_
#define _LED_H_

#include <stdbool.h>
#include <stdint.h>

typedef enum
{
  STATUS_LED,
  NUMBER_OF_LEDS,
} LED_TYPE;

/*!
 * @brief  Initializes all applicable LED's state and hardware.
 *
 * @param  None
 * @retval None
 *
 */
void Led_Init(void);

/*!
 * @brief  Turns off LEDs and makes pins ready for sleep.
 *
 * @param  None
 * @retval None
 *
 */
void Led_DeInit(void);

/*!
 * @brief  Turns on applicable LED.
 *
 * @param  [IN] index - LED_TYPE for what LED to turn on.
 * @retval bool
 *         true - Successfully turned on LED.
 *         false - Not initialized.
 *
 */
bool Led_On(LED_TYPE index);

/*!
 * @brief  Turns off applicable LED.
 *
 * @param  [IN] index - LED_TYPE for what LED to turn off.
 * @retval bool
 *         true - Successfully turned off LED.
 *         false - Not initialized.
 *
 */
bool Led_Off(LED_TYPE index);

/*!
 * @brief  Toggles applicable LED.
 *
 * @param  [IN] index - LED_TYPE for what LED to toggle.
 * @retval bool
 *         true - Successfully turned toggled LED.
 *         false - Not initialized.
 *
 */
bool Led_Toggle(LED_TYPE index);

#endif

