/*!
 *  datastore.h
 *
 *  Writes to EEPROM various settings for lora sensor device.
 *  Created on: Aug 9, 2019
 *  Author: Viaanix
 *
 */

#ifndef _DATASTORE_H_
#define _DATASTORE_H_

#include <stdint.h>
#include <stdbool.h>

#define MAGIC_NUMBER_BYTE_SIZE          (4)
#define DEV_EUI_BYTE_SIZE               (16)
#define APP_EUI_BYTE_SIZE               (16)
#define APP_KEY_BYTE_SIZE               (32)
#define OS_REPORT_RATE_BYTE_SIZE        (2)
#define FIRMWARE_VERSION_BYTE_SIZE      (2)

typedef struct
{
  int8_t dev_eui[DEV_EUI_BYTE_SIZE];
  int8_t app_eui[APP_EUI_BYTE_SIZE];
  int8_t app_key[APP_KEY_BYTE_SIZE];
  uint16_t os_report_rate;
  uint16_t firmware_version;
} datastore_settings_type;

/*!
 * @brief  Pulls initial settings from EEPROM if settings were set.
 *         Otherwise, uses defaults.
 *
 * @param  None
 * @retval None
 *
 */
void Datastore_Init(void);

/*!
 * @brief  Returns all the settings.
 *
 * @param  [IN] data - datastore_settings_type ptr to contain the info.
 * @retval bool
 *         true - data is valid.
 *         false - data is not valid.
 *
 */
bool Datastore_GetSettings(datastore_settings_type* data);

/*!
 * @brief  Saves settings into EEPROM. Typically went to get settings first with Datastore_GetSettings
 *
 * @param [IN] data - datastore_settings_type containing the new settings.
 * @retval bool
 *         true - Settings were saved.
 *         false - Datastore not initialized.
 *
 */
bool Datastore_SaveSettings(datastore_settings_type data);

#endif
