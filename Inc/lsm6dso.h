/*!
 * lsm6dso.h
 *
 * Handles reading and writing to the lsm6dso sensor.
 * Created on: Aug 14, 2019
 * Author: Viaanix
 *
 */

#ifndef _LSM6DSO_H_
#define _LSM6DSO_H_

#include <stdint.h>
#include <stdbool.h>

typedef struct
{
  uint16_t x;
  uint16_t y;
  uint16_t z;
} lsm6dso_acc_values_type;

typedef enum
{
  LSM6DSO_ACC_RANGE_2G = 0,
  LSM6DSO_ACC_RANGE_4G,
  LSM6DSO_ACC_RANGE_8G,
  LSM6DSO_ACC_RANGE_16G,
  NUMBER_OF_LSM6DSO_ACC_RANGE_TYPES
} LSM6DSO_ACC_RANGE_TYPE;

typedef enum
{
  LSM6DSO_ACC_ODR_POWER_DOWN,
  LSM6DSO_ACC_ODR_6_5HZ,
  LSM6DSO_ACC_ODR_12_5HZ,
  LSM6DSO_ACC_ODR_26HZ,
  LSM6DSO_ACC_ODR_52HZ,
  LSM6DSO_ACC_ODR_104HZ,
  LSM6DSO_ACC_ODR_208HZ,
  LSM6DSO_ACC_ODR_417HZ,
  LSM6DSO_ACC_ODR_833HZ,
  LSM6DSO_ACC_ODR_1_66KHZ,
  LSM6DSO_ACC_ODR_3_33KHZ,
  LSM6DSO_ACC_ODR_6_666KHZ,
  NUMBER_OF_LSM6DSO_ACC_ODR_TYPES
} LSM6DSO_ACC_ODR_TYPE;

typedef enum
{
  LSM6DSO_ACC_MODE_ULP,
  LSM6DSO_ACC_MODE_NORMAL,
  LSM6DSO_ACC_MODE_HP,
  NUMBER_OF_LSM6DSO_ACC_MODE_TYPES
} LSM6DSO_ACC_MODE_TYPE;

typedef struct
{
  LSM6DSO_ACC_RANGE_TYPE acc_range;
  LSM6DSO_ACC_MODE_TYPE acc_mode;
  LSM6DSO_ACC_ODR_TYPE acc_odr;
} lsm6dso_settings_type;

/*!
 * @brief  Checks to see if sensor exists and initializes it.
 *
 * @param  [IN] settings - lsm6dso_settings_type ptr containing the initial settings.
 *                         A value of NULL will keep the accelerometer in power down on default.
 * @retval bool
 *         true - initialization was successful
 *         false - somehow failed initialization.
 *
 */
bool Lsm6dso_Init(lsm6dso_settings_type* settings);

/*!
 * @brief  Gets snapshot of raw accelerometer data. Just reads once.
 *
 * @param  [IN] con - lsm6dso_acc_values_type ptr to hold the x, y, z raw values.
 * @retval bool
 *         true - valid data
 *         false - invalid data. Don't trust the values in con
 */
bool Lsm6dso_GetAccelerometerValues(lsm6dso_acc_values_type* con);

/*!
 * @brief  Updates the current settings of lis3hd sensor.
 *
 * @param  [IN] settings - lsm6dso_settings_type ptr containing the new settings.
 * @retval bool
 *         true - settings successfully updated.
 *         false - settings were not updated using previous settings.
 *
 */
bool Lsm6dso_UpdateSettings(lsm6dso_settings_type* settings);

/*!
 * @brief  Returns the current settings.
 *
 * @param [IN] settings - lsm6dso_settings_type ptr to hold the settings.
 * @retval bool
 *         true - settings in container are valid.
 *         false - settings in container are not valid.
 *
 */
bool Lsm6dso_GetSettings(lsm6dso_settings_type* settings);

#endif
