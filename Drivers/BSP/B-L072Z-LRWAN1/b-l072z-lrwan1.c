/**
  ******************************************************************************
  * @file    b-l072z-lrwan1.c
  * @author  MCD Application Team
  * @brief   This file contains definitions for:
  *          - LEDs and push-button available on B-L072Z-LRWAN1 Discovery Kit 
  *            from STMicroelectronics
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2018 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

  
/* Includes ------------------------------------------------------------------*/
#include "b-l072z-lrwan1.h"
#include <stdlib.h>

/** @addtogroup BSP
  * @{
  */ 

/** @addtogroup B-L072Z-LRWAN1
  * @{
  */   
    
/** @addtogroup B-L072Z-LRWAN1_LOW_LEVEL 
  * @brief This file provides set of firmware functions to manage Leds and push-button
  *        available on B-L072Z-LRWAN1 Discovery Kit from STMicroelectronics.
  * @{
  */ 

/** @defgroup B-L072Z-LRWAN1_LOW_LEVEL_Private_TypesDefinitions 
  * @{
  */ 
/**
  * @}
  */ 


/** @defgroup B-L072Z-LRWAN1_LOW_LEVEL_Private_Defines 
  * @{
  */ 

/**
  * @brief 32L082MLM DISCO BSP Driver version number V1.0.0
  */
#define __B_L072Z_LRWAN1_BSP_VERSION_MAIN   (0x01) /*!< [31:24] main version */
#define __B_L072Z_LRWAN1_BSP_VERSION_SUB1   (0x00) /*!< [23:16] sub1 version */
#define __B_L072Z_LRWAN1_BSP_VERSION_SUB2   (0x00) /*!< [15:8]  sub2 version */
#define __B_L072Z_LRWAN1_BSP_VERSION_RC     (0x00) /*!< [7:0]  release candidate */
#define __B_L072Z_LRWAN1_BSP_VERSION         ((__B_L072Z_LRWAN1_BSP_VERSION_MAIN << 24)\
                                             |(__B_L072Z_LRWAN1_BSP_VERSION_SUB1 << 16)\
                                             |(__B_L072Z_LRWAN1_BSP_VERSION_SUB2 << 8 )\
                                             |(__B_L072Z_LRWAN1_BSP_VERSION_RC))



/** @defgroup B-L072Z-LRWAN1_LOW_LEVEL_Private_Variables
  * @{
  */ 
GPIO_TypeDef* LED_PORT[LEDn] = {LED1_GPIO_PORT, LED2_GPIO_PORT, LED3_GPIO_PORT, LED4_GPIO_PORT};
const uint16_t LED_PIN[LEDn] = {LED1_PIN, LED2_PIN,LED3_PIN, LED4_PIN};

/**
  * @}
  */ 
  

/** @defgroup B-L072Z-LRWAN1_LOW_LEVEL_Private_Functions
  * @{
  */ 
  /**
  * @}
  */
/**
  * @brief  This method returns the B-L072Z-LRWAN1 BSP Driver revision
  * @param  None
  * @retval version : 0xXYZR (8bits for each decimal, R for RC)
  */
uint32_t BSP_GetVersion(void)
{
  return __B_L072Z_LRWAN1_BSP_VERSION;
}

/**
  * @brief  Configures LED GPIO.
  * @param  Led: Specifies the Led to be configured. 
  *   This parameter can be one of following parameters:
  *            @arg  LED2
  * @retval None
  */
void BSP_LED_Init(Led_TypeDef Led)
{
  GPIO_InitTypeDef  GPIO_InitStruct;
  
  /* Enable the GPIO_LED Clock */
  LEDx_GPIO_CLK_ENABLE( Led );

  /* Configure the GPIO_LED pin */
  GPIO_InitStruct.Pin = LED_PIN[Led];
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  
  HAL_GPIO_Init(LED_PORT[Led], &GPIO_InitStruct);
}

/**
  * @brief  Turns selected LED On.
  * @param  Led: Specifies the Led to be set on. 
  *   This parameter can be one of following parameters:
  *            @arg  LED2
  * @retval None
  */
void BSP_LED_On(Led_TypeDef Led)
{
  HAL_GPIO_WritePin(LED_PORT[Led], LED_PIN[Led], GPIO_PIN_SET); 
}

/**
  * @brief  Turns selected LED Off. 
  * @param  Led: Specifies the Led to be set off. 
  *   This parameter can be one of following parameters:
  *            @arg  LED2
  * @retval None
  */
void BSP_LED_Off(Led_TypeDef Led)
{
  HAL_GPIO_WritePin(LED_PORT[Led], LED_PIN[Led], GPIO_PIN_RESET); 
}

/**
  * @brief  Toggles the selected LED.
  * @param  Led: Specifies the Led to be toggled. 
  *   This parameter can be one of following parameters:
  *            @arg  LED2
  * @retval None
  */
void BSP_LED_Toggle(Led_TypeDef Led)
{
  HAL_GPIO_TogglePin(LED_PORT[Led], LED_PIN[Led]);
}

/**
  * @}
  */ 

/**
  * @}
  */

/**
  * @}
  */    

/**
  * @}
  */ 
    
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
