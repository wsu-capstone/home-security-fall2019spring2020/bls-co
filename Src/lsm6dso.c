/*!
 * lsm6dso.c
 *
 * Handles reading and writing to the lsm6dso sensor.
 * Created on: Aug 14, 2019
 * Author: Viaanix
 *
 */

#include "lsm6dso.h"
#include "hw_i2c.h"
#include "lsm6dso_reg.h"

static bool Lsm6dsoInit = false;
static lsm6dso_ctx_t Lsm6dso;
static lsm6dso_settings_type Lsm6dsoSettings;

static lsm6dso_fs_xl_t Lsm6dsoAccRange[NUMBER_OF_LSM6DSO_ACC_RANGE_TYPES] =
{
  LSM6DSO_2g,
  LSM6DSO_4g,
  LSM6DSO_8g,
  LSM6DSO_16g
};

static lsm6dso_xl_hm_mode_t Lsm6dsoAccMode[NUMBER_OF_LSM6DSO_ACC_MODE_TYPES] =
{
  LSM6DSO_ULTRA_LOW_POWER_MD,
  LSM6DSO_LOW_NORMAL_POWER_MD,
  LSM6DSO_HIGH_PERFORMANCE_MD
};

static lsm6dso_odr_xl_t Lsm6dsoAccOdr[NUMBER_OF_LSM6DSO_ACC_ODR_TYPES] =
{
  LSM6DSO_XL_ODR_OFF,
  LSM6DSO_XL_ODR_6Hz5,
  LSM6DSO_XL_ODR_12Hz5,
  LSM6DSO_XL_ODR_26Hz,
  LSM6DSO_XL_ODR_52Hz,
  LSM6DSO_XL_ODR_104Hz,
  LSM6DSO_XL_ODR_208Hz,
  LSM6DSO_XL_ODR_417Hz,
  LSM6DSO_XL_ODR_833Hz,
  LSM6DSO_XL_ODR_1667Hz,
  LSM6DSO_XL_ODR_3333Hz,
  LSM6DSO_XL_ODR_6667Hz
};

static int32_t lsm6dsoWrite(void *handle, uint8_t reg, uint8_t *bufp, uint16_t len);
static int32_t lsm6dsoRead(void *handle, uint8_t reg, uint8_t *bufp, uint16_t len);
static bool lsm6dsoValidateSettings(lsm6dso_settings_type* settings);
static bool lsm6dsoUpdateSettings(lsm6dso_settings_type* settings);

/*=== Lsm6dso_Init ===*/
bool Lsm6dso_Init(lsm6dso_settings_type* settings)
{
  if(Lsm6dsoInit == false)
  {
    uint8_t who_am_i = 0;
    Lsm6dso.write_reg = lsm6dsoWrite;
    Lsm6dso.read_reg = lsm6dsoRead;
    Lsm6dso.handle = &hi2c2;

    lsm6dso_device_id_get(&Lsm6dso, &who_am_i);
    if(who_am_i == LSM6DSO_ID)
    {
      Lsm6dsoInit = true;

      if(Lsm6dso_UpdateSettings(settings) == false)
      {
        Lsm6dsoInit = false;
        return false;
      }
    }
    else
    {
      return false;
    }

    return true;
  }

  return true;
}

/*=== Lsm6dso_GetAccelerometerValues ===*/
bool Lsm6dso_GetAccelerometerValues(lsm6dso_acc_values_type* con)
{
  uint8_t buff[6];
  if(Lsm6dsoInit == false)
  {
    return false;
  }

  if(lsm6dso_acceleration_raw_get(&Lsm6dso, buff) == false)
  {
    return false;
  }
  else
  {
    con->x = ((uint16_t)buff[0]) + (((uint16_t)buff[1])<<8);
    con->y = ((uint16_t)buff[2]) + (((uint16_t)buff[3])<<8);
    con->z = ((uint16_t)buff[4]) + (((uint16_t)buff[5])<<8);
  }
  return true;
}

/*=== Lsm6dso_UpdateSettings ===*/
bool Lsm6dso_UpdateSettings(lsm6dso_settings_type* settings)
{
  if(Lsm6dsoInit == false)
  {
    return false;
  }

  if(settings == NULL)
  {
    return false;
  }

  if(lsm6dsoValidateSettings(settings) == false)
  {
    return false;
  }

  if(lsm6dsoUpdateSettings(settings) == false)
  {
    return false;
  }

  Lsm6dsoSettings = *settings;
  return true;
}

/*=== Lsm6dso_GetSettings ===*/
bool Lsm6dso_GetSettings(lsm6dso_settings_type* settings)
{
  if(Lsm6dsoInit == false)
  {
    return false;
  }

  *settings = Lsm6dsoSettings;
  return true;
}

/*
 * @brief  Write generic device register (platform dependent)
 *
 * @param  [IN] handle - customizable argument. In this examples is used in
 *                       order to select the correct sensor bus handler.
 * @param  [IN] reg - register to write
 * @param  [IN] bufp - pointer to data to write in register reg
 * @param  [IN] len - number of consecutive register to write
 * @retval int32_t
 *         0 - success
 *         1 - failure
 */
static int32_t lsm6dsoWrite(void *handle, uint8_t reg, uint8_t *bufp,
                              uint16_t len)
{
  if (handle == &hi2c2)
  {
    /* Write multiple command */
    reg |= 0x80;
    HAL_I2C_Mem_Write(handle, LSM6DSO_I2C_ADD_L, reg,
                      I2C_MEMADD_SIZE_8BIT, bufp, len, 1000);
    return (uint32_t)0;
  }
  return (uint32_t)1;
}

/*
 * @brief  Read generic device register (platform dependent)
 *
 * @param  [IN] handle - customizable argument. In this examples is used in
 *                       order to select the correct sensor bus handler.
 * @param  [IN] reg - register to read
 * @param  [IN] bufp - pointer to data to read in register reg
 * @param  [IN] len - number of consecutive register to read
 * @retval int32_t
 *         0 - success
 *         1 - failure
 */
static int32_t lsm6dsoRead(void *handle, uint8_t reg, uint8_t *bufp,
                              uint16_t len)
{
  if (handle == &hi2c2)
  {
    /* Read multiple command */
    reg |= 0x80;
    HAL_I2C_Mem_Read(handle, LSM6DSO_I2C_ADD_L, reg,
                     I2C_MEMADD_SIZE_8BIT, bufp, len, 1000);
    return (uint32_t)0;
  }
  return (uint32_t)1;
}

/*!
 * @brief  Makes sure all settings provided are valid values.
 *
 * @param  [IN] settings - lsm6dso_settings_type settings to validate.
 * @retval bool
 *         true - settings are valid
 *         false - at least one setting is invalid
 *
 */
static bool lsm6dsoValidateSettings(lsm6dso_settings_type* settings)
{
  if(settings->acc_range >= NUMBER_OF_LSM6DSO_ACC_RANGE_TYPES)
  {
    return false;
  }

  if(settings->acc_odr >= NUMBER_OF_LSM6DSO_ACC_ODR_TYPES)
  {
    return false;
  }

  if(settings->acc_mode >= NUMBER_OF_LSM6DSO_ACC_MODE_TYPES)
  {
    return false;
  }

  return true;
}

/*!
 * @brief  Does the actual writing to registers to update the settings.
 *
 * @param  settings = lsm6dso_settings_type ptr containing the settings to change to.
 * @retval bool
 *         true - Settings successfully were updated.
 *         false - At least one setting failed to update.
 *
 */
static bool lsm6dsoUpdateSettings(lsm6dso_settings_type* settings)
{
  uint32_t failure_status = 0;

  failure_status += lsm6dso_xl_full_scale_set(&Lsm6dso, Lsm6dsoAccRange[settings->acc_range]);
  failure_status += lsm6dso_xl_power_mode_set(&Lsm6dso, Lsm6dsoAccMode[settings->acc_mode]);
  failure_status += lsm6dso_xl_data_rate_set(&Lsm6dso, Lsm6dsoAccOdr[settings->acc_odr]);

  if(failure_status)
  {
    return false;
  }

  return true;
}
