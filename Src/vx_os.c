/*
 *  vx_os.c
 *
 *  Handles collecting battery, temperature data and sends via LoRaWAN.
 *  Created on: Aug 7, 2019
 *  Author: Viaanix
 *
 */

#include "vx_os.h"
#include "battery.h"
#include "datastore.h"
#include "fifo.h"
#include "temperature.h"
#include "carbonmonoxide.h"
#include "timeServer.h"

#define VX_OS_FIRMWARE_VERSION               (1)
#define VX_OS_CHECK_IN_TIME_MINUTES_MIN      (1)
#define VX_OS_CHECK_IN_TIME_MINUTES_MAX      (1440)
#define VX_OS_DEFAULT_CHECK_IN_TIME_MINUTES  (1)
#define VX_OS_MAX_QUEUE_SIZE                 (4)

#define VX_OS_CARBONMONOXIDE_NOT_VALID          (0xFFFF)
#define VX_OS_TEMPERATURE_NOT_VALID          (0xFFFF)
#define VX_OS_BATTERY_NOT_VALID              (0xFFFF)
#define VX_OS_FIRMWARE_VERSION_NOT_VALID     (0x0000)

#define VX_OS_EXPECTED_RESPONSE_SIZE         (3)

typedef enum
{
  VX_OS_STATE_WAIT,
  VX_OS_STATE_GET_BATTERY,
  VX_OS_STATE_GET_TEMPERATURE,
  VX_OS_STATE_GET_CARBONMONOXIDE,
  VX_OS_STATE_QUEUE_DATA,
  VX_OS_STATE_ERROR
} VX_OS_STATE_TYPE;

typedef enum
{
  VX_OS_NO_ERROR,
  VX_OS_ERROR_FIFO_FULL,
} VX_OS_ERROR_TYPE;

static bool VxOsInit = false;
static VX_OS_STATE_TYPE VxOsState = VX_OS_STATE_WAIT;
static VX_OS_ERROR_TYPE VxOsErrorState = VX_OS_NO_ERROR;
static bool VxOsStartStateMachineActive = false;
static TimerEvent_t VxOsTimer;
static vx_os_data_type VxOsData = { 0 };
static vx_os_setting_type VxOsSettings = { 0 };

static void vxOsTimerEvent(void* context);
static void vxOsGetDefaultSettings(void);
static void vxOsResetData(void);
static void vxOsHandleWaitState(void);
static void vxOsHandleGetBatteryState(void);
static void vxOsHandleGetTemperatureState(void);
static void vxOsHandleGetCarbonmonoxideState(void);
static void vxOsHandleQueueDataState(void);
static void vxOsHandleErrorState(void);
AddPointerFifo(vxOsTx,VX_OS_MAX_QUEUE_SIZE,vx_os_data_type,true,false);

/* VxOs_Init */
void VxOs_Init(void)
{
  if(VxOsInit == false)
  {
    vxOsTxFifo_Init();
    vxOsGetDefaultSettings();

    /* Send every time timer elapses */
    TimerInit(&VxOsTimer, vxOsTimerEvent);
    VxOsInit = true;
  }
}

/* VxOs_Process */
void VxOs_Process(void)
{
  if(VxOsInit == true)
  {
    switch(VxOsState)
    {
      case VX_OS_STATE_WAIT:
      {
        vxOsHandleWaitState();
        break;
      }

      case VX_OS_STATE_GET_BATTERY:
      {
        vxOsHandleGetBatteryState();
        break;
      }

      case VX_OS_STATE_GET_TEMPERATURE:
      {
        vxOsHandleGetTemperatureState();
        break;
      }

      case VX_OS_STATE_GET_CARBONMONOXIDE:
      {
        vxOsHandleGetCarbonmonoxideState();
        break;
      }

      case VX_OS_STATE_QUEUE_DATA:
      {
        vxOsHandleQueueDataState();
        break;
      }

      case VX_OS_STATE_ERROR:
      {
        vxOsHandleErrorState();
        break;
      }
    }
  }
}

/*=== VxOs_IsDataToSend ===*/
bool VxOs_IsDataToSend(vx_os_data_type* data)
{
  if(VxOsInit == false)
  {
    return false;
  }

  if(vxOsTxFifo_Get(data))
  {
    return true;
  }
  else
  {
    return false;
  }
}

/*=== VxOs_HandleRx ===*/
bool VxOs_HandleRx(uint8_t* data, uint8_t size)
{
  vx_os_setting_type setting;
  datastore_settings_type ds;
  if(VxOsInit == false)
  {
    return false;
  }

  if(size < VX_OS_EXPECTED_RESPONSE_SIZE)
  {
    return false;
  }

  setting.check_in_time_minutes = (((uint16_t)data[1])<<8) + ((uint16_t)data[2]);
  if(setting.check_in_time_minutes >= VX_OS_CHECK_IN_TIME_MINUTES_MIN &&
     setting.check_in_time_minutes <= VX_OS_CHECK_IN_TIME_MINUTES_MAX)
  {
    VxOsSettings.check_in_time_minutes = setting.check_in_time_minutes;
  }

  Datastore_GetSettings(&ds);
  ds.os_report_rate = VxOsSettings.check_in_time_minutes;
  Datastore_SaveSettings(ds);

  VxOs_StopProcess();
  VxOs_StartProcess();
  return true;
}

/*=== VxOs_StartProcess ===*/
void VxOs_StartProcess(void)
{
  if(VxOsInit == false)
  {
    return;
  }

  vxOsTimerEvent(NULL);
}

/*=== VxOs_StopProcess ===*/
void VxOs_StopProcess(void)
{
  if(VxOsInit == false)
  {
    return;
  }

  TimerStop(&VxOsTimer);
}

/*=== VxOs_IsBusy ===*/
bool VxOs_IsBusy(void)
{
  if(VxOsInit == false)
  {
    return false;
  }

  if(VxOsStartStateMachineActive == true)
  {
    return true;
  }

  return false;
}

/*!
 * @brief  Sets flag to let main process know to start machine.
 *         Uses the global: VxOsStartStateMachine.
 *
 * @param  None
 * @retval None
 *
 */
static void vxOsTimerEvent(void* context)
{
  TimerSetValue(&VxOsTimer,
               ((uint32_t)VxOsSettings.check_in_time_minutes)*MINUTES_MULTIPLIER);
  TimerStart(&VxOsTimer);
  VxOsStartStateMachineActive = true;
}

/*!
 * @brief  Sets OS Settings. Checks EEPROM for saved settings.
 *         If EEPROM is not set, then use default from defines.
 *
 * @param  None
 * @retval None
 *
 */
static void vxOsGetDefaultSettings(void)
{
  datastore_settings_type data;
  if(Datastore_GetSettings(&data) == true)
  {
    VxOsSettings.check_in_time_minutes = data.os_report_rate;
  }
  else
  {
    VxOsSettings.check_in_time_minutes = VX_OS_DEFAULT_CHECK_IN_TIME_MINUTES;
  }

  VxOsSettings.check_in_time_minutes = MAX(VxOsSettings.check_in_time_minutes, VX_OS_CHECK_IN_TIME_MINUTES_MIN);
  VxOsSettings.check_in_time_minutes = MIN(VxOsSettings.check_in_time_minutes, VX_OS_CHECK_IN_TIME_MINUTES_MAX);
}

/*!
 * @brief  Resets Data global that gets queued.
 *
 * @param  None
 * @retval None
 *
 */
static void vxOsResetData(void)
{
  datastore_settings_type data;
  if(Datastore_GetSettings(&data) == true)
  {
    VxOsData.firmware_version = data.firmware_version;
  }
  else
  {
    VxOsData.firmware_version = VX_OS_FIRMWARE_VERSION;
  }

  VxOsData.battery_adc = VX_OS_BATTERY_NOT_VALID;
  VxOsData.temperature_adc = VX_OS_TEMPERATURE_NOT_VALID;
  VxOsData.carbonmonoxide_adc = VX_OS_CARBONMONOXIDE_NOT_VALID;
}

/*!
 * @brief  Waits until timer VxOsTimer sets VxOsStartStateMachine
 *
 * @param  None
 * @retval None
 *
 */
static void vxOsHandleWaitState(void)
{
  if(VxOsInit == false)
  {
    VxOsState = VX_OS_STATE_WAIT;
    VxOsStartStateMachineActive = false;
    return;
  }

  if(VxOsStartStateMachineActive == true)
  {
    VxOsState = VX_OS_STATE_GET_BATTERY;
    VxOsErrorState = VX_OS_NO_ERROR;
    vxOsResetData();
  }
}

/*!
 * @brief  Handles state to pull battery voltage from uC and put in data to queue.
 *
 * @param  None
 * @retval None
 *
 */
static void vxOsHandleGetBatteryState(void)
{
  if(VxOsInit == false)
  {
    VxOsState = VX_OS_STATE_WAIT;
    VxOsStartStateMachineActive = false;
    return;
  }

  //Call for Battery ADC
  battery_data_type battery;
  if(Battery_GetData(&battery) == true)
  {
    VxOsData.battery_adc = battery.adc;
    VxOsState = VX_OS_STATE_GET_TEMPERATURE;
  }

  //Call for Battery Level
  if(Battery_GetLevel(&battery) == true)
  {
    VxOsData.battery_level = battery.adc;
    VxOsState = VX_OS_STATE_GET_TEMPERATURE;
  }
}

/*!
 * @brief  Handles state to pull temperature from uC and put in data to queue.
 *         This state relies on reference voltage calculated in VX_OS_STATE_GET_BATTERY
 */
static void vxOsHandleGetTemperatureState(void)
{
  if(VxOsInit == false)
  {
    VxOsState = VX_OS_STATE_WAIT;
    VxOsStartStateMachineActive = false;
    return;
  }

  temperature_data_type temperature;
  if(Temperature_GetData(&temperature) == true)
  {
    VxOsData.temperature_adc = temperature.adc;
    VxOsState = VX_OS_STATE_GET_CARBONMONOXIDE;
  }
}

/*!
 * @brief  Handles state to pull carbon monoxide
 *
 */
static void vxOsHandleGetCarbonmonoxideState(void)
{
  if(VxOsInit == false)
  {
    VxOsState = VX_OS_STATE_WAIT;
    VxOsStartStateMachineActive = false;
    return;
  }

  //Call for Carbon Monoxide ADC
  carbonmonoxide_data_type carbonmonoxide;
  if(Carbonmonoxide_GetData(&carbonmonoxide) == true)
  {
    VxOsData.carbonmonoxide_adc = carbonmonoxide.adc;
    VxOsState = VX_OS_STATE_QUEUE_DATA;
  }

  //Call for Carbon Monoxide Level
  if(Carbonmonoxide_GetLevel(&carbonmonoxide) == true)
  {
    VxOsData.carbonmonoxide_level = carbonmonoxide.adc;
    VxOsState = VX_OS_STATE_QUEUE_DATA;
  }
}

static void vxOsHandleQueueDataState(void)
{
  if(VxOsInit == false)
  {
    VxOsState = VX_OS_STATE_WAIT;
    VxOsStartStateMachineActive = false;
    return;
  }

  if(vxOsTxFifo_Put(VxOsData))
  {
    VxOsState = VX_OS_STATE_WAIT;
    VxOsStartStateMachineActive = false;
  }
  else
  {
    VxOsState = VX_OS_STATE_ERROR;
  }
}

static void vxOsHandleErrorState(void)
{
  if(VxOsInit == false)
  {
    VxOsState = VX_OS_STATE_WAIT;
    VxOsStartStateMachineActive = false;
    return;
  }

  VxOsState = VX_OS_STATE_WAIT;
  VxOsStartStateMachineActive = false;
}
