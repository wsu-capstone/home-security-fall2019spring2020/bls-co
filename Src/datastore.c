/*
 *  datastore.c
 *
 *  Writes to EEPROM various settings for lora sensor device.
 *  Created on: Aug 9, 2019
 *  Author: Viaanix
 *
 */

#include <string.h>
#include "datastore.h"
#include "hw.h"

#define DATASTORE_ERASE_EEPROM

#define DATASTORE_EEPROM_START_ADDR     0x08080000
#define DATASTORE_EEPROM_END_ADDR       0x080817FF
#define DATASTORE_EEPROM_SIZE_BYTES     (DATASTORE_EEPROM_END_ADDR-DATASTORE_EEPROM_START_ADDR)
#define DATASTORE_MAX_VARIABLE_SIZE     (64)

#define MAGIC_NUMBER_INDEX              (0)
#define DEV_EUI_INDEX                   (MAGIC_NUMBER_INDEX+MAGIC_NUMBER_BYTE_SIZE)
#define APP_EUI_INDEX                   (DEV_EUI_INDEX+DEV_EUI_BYTE_SIZE)
#define APP_KEY_INDEX                   (APP_EUI_INDEX+APP_EUI_BYTE_SIZE)
#define OS_REPORT_RATE_INDEX            (APP_KEY_INDEX+APP_KEY_BYTE_SIZE)
#define FIRMWARE_VERSION_INDEX          (OS_REPORT_RATE_INDEX+OS_REPORT_RATE_BYTE_SIZE)

static bool DatastoreInit = false;
static uint8_t MagicNumber[MAGIC_NUMBER_BYTE_SIZE] = { 'V', 'X', 'L', 0x17 };
static datastore_settings_type CurrentDatastoreData;

static datastore_settings_type DefaultDatastoreData =
{
    .dev_eui = "1220180003975FF0",
    .app_eui = "70B3D57ED0023388",
    .app_key = "891C2B90D43FFCF1740B30D1840EA649",
    .firmware_version = 4,
    .os_report_rate = 1
};

static bool datastoreRead(uint32_t index, uint8_t *data , uint16_t size);
static bool datastoreWriteSeq(uint32_t index, uint8_t *data, uint16_t size);
static bool datastoreIsDataValid(void);
static void datastoreLoadDataFromEeprom(void);
static void datastoreSaveSettingsInEeprom(void);
static void datastoreEraseEeprom(void);

/*=== Datastore_Init ===*/
void Datastore_Init(void)
{
  if(DatastoreInit == false)
  {
    datastoreEraseEeprom();

    if(datastoreIsDataValid() == true)
    {
      datastoreLoadDataFromEeprom();
    }
    else
    {
      memcpy(CurrentDatastoreData.dev_eui, DefaultDatastoreData.dev_eui, DEV_EUI_BYTE_SIZE);
      memcpy(CurrentDatastoreData.app_eui, DefaultDatastoreData.app_eui, APP_EUI_BYTE_SIZE);
      memcpy(CurrentDatastoreData.app_key, DefaultDatastoreData.app_key, APP_KEY_BYTE_SIZE);
      CurrentDatastoreData.firmware_version = DefaultDatastoreData.firmware_version;
      CurrentDatastoreData.os_report_rate = DefaultDatastoreData.os_report_rate;
      datastoreSaveSettingsInEeprom();
    }
    DatastoreInit = true;
  }
}

/*=== Datastore_GetSettings ===*/
bool Datastore_GetSettings(datastore_settings_type* data)
{
  if(DatastoreInit == false)
  {
    return false;
  }

  *data = CurrentDatastoreData;
  return true;
}

/*=== Datastore_SaveSettings ===*/
bool Datastore_SaveSettings(datastore_settings_type data)
{
  if(DatastoreInit == false)
  {
    return false;
  }

  CurrentDatastoreData = data;
  datastoreSaveSettingsInEeprom();
  return true;
}

/*!
 * @brief  Reads a section of memory from EEPROM.
 *
 * @param  [IN] index - uint32_t containing the offset index to start reading.
 * @param  [IN] data - uint8_t ptr to buffer to hold read data.
 * @param  [IN] size - uint16_t number of bytes to read.
 * @retval bool
 *         true - data successfully read
 *         false - index out of range
 *
 */
static bool datastoreRead(uint32_t index, uint8_t *data , uint16_t size)
{
  index = index + DATASTORE_EEPROM_START_ADDR;
  if (index > DATASTORE_EEPROM_END_ADDR || size > DATASTORE_EEPROM_SIZE_BYTES)
  {
    return false;
  }
  uint8_t *src = (uint8_t*)index;
  memcpy(data,src,size);
  return true;
}

/*!
 * @brief  Writes data sequentially in EEPROM
 *
 * @param  [IN] index - uint32_t containing offset of where to start writing.
 * @param  [IN] data - uint8_t ptr to list of data to write.
 * @param  [IN] size - uint16_t amount of bytes to write.
 * @retval bool
 *         true - If all data successfully wrote.
 *         false - failed to write at least one byte.
 *
 */
static bool datastoreWriteSeq(uint32_t index, uint8_t *data, uint16_t size)
{
  HAL_StatusTypeDef status = HAL_OK;
  index = index + DATASTORE_EEPROM_START_ADDR;

  HAL_FLASHEx_DATAEEPROM_Unlock();
  for (uint32_t i = 0; i < size; i++)
  {
    status = HAL_FLASHEx_DATAEEPROM_Program(TYPEPROGRAMDATA_BYTE ,index+i, data[i]);
    if(status != HAL_OK)
    {
      break;
    }
  }

  HAL_FLASHEx_DATAEEPROM_Lock();

  if(status != HAL_OK)
  {
    return false;
  }

  return true;
}

/*!
 * @brief  Checks to see if magic number is valid. If so, then EEPROM data is valid.
 *
 * @param  None
 * @retval bool
 *         true - Data is valid in EEPROM.
 *         false - Data is not valid in EERPOM.
 *
 */
static bool datastoreIsDataValid(void)
{
  uint8_t magic_number[MAGIC_NUMBER_BYTE_SIZE];
  if(datastoreRead(MAGIC_NUMBER_INDEX, magic_number, MAGIC_NUMBER_BYTE_SIZE) == true)
  {
    if(memcmp(magic_number, MagicNumber, MAGIC_NUMBER_BYTE_SIZE) == 0)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  else
  {
    return false;
  }
}

/*!
 * @brief  Loads all settings from EEPROM.
 *
 * @param  None
 * @retval None
 *
 */
static void datastoreLoadDataFromEeprom(void)
{
  uint8_t data[DATASTORE_MAX_VARIABLE_SIZE];

  if(datastoreRead(DEV_EUI_INDEX, data, DEV_EUI_BYTE_SIZE) == true)
  {
    memcpy(CurrentDatastoreData.dev_eui, data, DEV_EUI_BYTE_SIZE);
  }

  if(datastoreRead(APP_EUI_INDEX, data, APP_EUI_BYTE_SIZE) == true)
  {
    memcpy(CurrentDatastoreData.app_eui, data, APP_EUI_BYTE_SIZE);
  }

  if(datastoreRead(APP_KEY_INDEX, data, APP_KEY_BYTE_SIZE) == true)
  {
    memcpy(CurrentDatastoreData.app_key, data, APP_KEY_BYTE_SIZE);
  }

  if(datastoreRead(OS_REPORT_RATE_INDEX, data, OS_REPORT_RATE_BYTE_SIZE) == true)
  {
    CurrentDatastoreData.os_report_rate = (((uint16_t)data[0])<<8) + ((uint16_t)data[1]);
  }

  if(datastoreRead(FIRMWARE_VERSION_INDEX, data, FIRMWARE_VERSION_BYTE_SIZE) == true)
  {
    CurrentDatastoreData.firmware_version = (((uint16_t)data[0])<<8) + ((uint16_t)data[1]);
  }
}

/*!
 * @brief  Saves current settings into EEPROM.
 *
 * @param  None
 * @retval None
 *
 */
static void datastoreSaveSettingsInEeprom(void)
{
  uint8_t data[DATASTORE_MAX_VARIABLE_SIZE];

  datastoreWriteSeq(DEV_EUI_INDEX, (uint8_t*)CurrentDatastoreData.dev_eui, DEV_EUI_BYTE_SIZE);
  datastoreWriteSeq(APP_EUI_INDEX, (uint8_t*)CurrentDatastoreData.app_eui, APP_EUI_BYTE_SIZE);
  datastoreWriteSeq(APP_KEY_INDEX, (uint8_t*)CurrentDatastoreData.app_key, APP_KEY_BYTE_SIZE);

  data[0] = (CurrentDatastoreData.firmware_version&0xFF00)>>8;
  data[1] = (CurrentDatastoreData.firmware_version&0x00FF);
  datastoreWriteSeq(FIRMWARE_VERSION_INDEX, data, FIRMWARE_VERSION_BYTE_SIZE);

  data[0] = (CurrentDatastoreData.os_report_rate&0xFF00)>>8;
  data[1] = (CurrentDatastoreData.os_report_rate&0x00FF);
  datastoreWriteSeq(OS_REPORT_RATE_INDEX, data, OS_REPORT_RATE_BYTE_SIZE);

  if(datastoreIsDataValid() == false)
  {
    datastoreWriteSeq(MAGIC_NUMBER_INDEX, MagicNumber, MAGIC_NUMBER_BYTE_SIZE);
  }
}

/*!
 * @brief  Erases the magic number to invalidate EEPROM.
 *
 * @param  None
 * @retval None
 *
 */
static void datastoreEraseEeprom(void)
{
#ifdef DATASTORE_ERASE_EEPROM
  uint8_t magic_number[MAGIC_NUMBER_BYTE_SIZE];

  memset(magic_number, 0, MAGIC_NUMBER_BYTE_SIZE);
  datastoreWriteSeq(MAGIC_NUMBER_INDEX, magic_number, MAGIC_NUMBER_BYTE_SIZE);
#endif
}
